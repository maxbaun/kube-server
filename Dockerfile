FROM node:10-alpine
WORKDIR /app
COPY package.json ./
RUN npm install && npm cache clean --force
COPY . .
CMD ["node", "index.js"]
